package nixautoframework.spec
import geb.spock.GebReportingSpec
import nixautoframework.page.*
class NixNavigationSpec extends GebReportingSpec {

    def "Navigate to Blog page"() {
        when:
        to StartPage

        and: "User navigates to Blog page"()

        then:
        at BlogPage
    }
}