package nixautoframework.page

import geb.Page

class StartPage extends Page {
static content = {
    productsLink (wait:true) { $("a", text: "Blog") }
}

static at = {
    title == "NIX – Outsourcing Offshore Software Development Company"
}
 def "User navigates to Blog page"() {
     productsLink.click()
 }
}
